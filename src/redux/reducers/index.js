import AuthReducer from './AuthReducers'
import {combineReducers} from 'redux'

export default combineReducers({
    Auth: AuthReducer,
})
