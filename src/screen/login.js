

import React, { useState } from 'react';
import { View, Image,StyleSheet,TextInput,Button,
    KeyboardAvoidingView,Keyboard,Platform,TouchableWithoutFeedback,ScrollView
} from 'react-native';
import { useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

const Appinit = () => {

    let dispatch = useDispatch();

    const [username,onChangeUsername] = useState('')
    const [password,onChangePassword] = useState('')

    const onPressLogin=()=>{
        console.log(username)
        console.log(password)
        
        dispatch({ type: 'USER_LOGIN' ,payload: username})
        AsyncStorage.setItem('username',username)  
      }

    return (
        <ScrollView>

            <KeyboardAvoidingView
                behavior={Platform.OS == "android" ? 40 : 40}
                style={styles.container}
            >
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            
            <View style={styles.container}>
                
                <Image
                    style={styles.logo}
                    source={require('../../picture/logo.png')}
                >
                </Image>

                <TextInput
                    style={styles.textInputUsername}
                    onChangeText={text => onChangeUsername(text)}
                    placeholder='Masukkan Username'
                />

                <TextInput
                    secureTextEntry={true}
                    style={styles.textInputPassword}
                    onChangeText={text => onChangePassword(text)}
                    placeholder='Masukkan Password'
                />

                <View style={styles.button}>
                    <Button
                        title="Login"
                        color="#3DDC84"
                        onPress={onPressLogin}
                    />
                </View>

            </View>
            </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </ScrollView>

    )
}

const styles = StyleSheet.create({
    container:{
        marginTop:'10%'
    },
    logo: {
        width: 201,
        height: 300,
        alignSelf:'center'
      },
    textInputUsername:{
        marginTop:40,
        width: 200, 
        borderColor: "#3DDC84", 
        borderWidth: 0.5, 
        alignSelf:'center',
        textAlign:'center'
    },
    textInputPassword:{
        marginTop:20,
        width: 200, 
        borderColor: "#3DDC84", 
        borderWidth: 0.5, 
        alignSelf:'center',
        textAlign:'center'
    },
    button: {
        width:100,
        marginTop:20,
        alignSelf:'center'
    },
      
})

export default Appinit;