import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';

const Profile = () => {
    
    let defaultpic = useSelector(state => state.Auth.defaultpic)
    let username = useSelector(state => state.Auth.username)

    return (
        <View style={styles.container}>
            <Image source={{uri: defaultpic}} style={styles.image}>
            </Image> 

            <Text style={{marginTop:10}}>
                Halo! {username}
            </Text>
            <Text style={{marginLeft:40,marginRight:40,marginTop:10,textAlign:'center'}}>
                Silahkan Pilih Gambar Kucing yang anda inginkan untuk menjadi foto profil.
            </Text>

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        height:'100%',
        justifyContent: 'center',
        alignItems:'center'
    },
    image: {
        borderWidth: 1,
        borderRadius: 100,
        borderColor:'#3DDC84',
        width: 300,
        height: 300
    }
})

export default Profile;