import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Ionicons from 'react-native-vector-icons/Ionicons';

import Home from '../screen/home'
import ProfileDrawer from '../navigation/ProfileDrawer'

const Tab=createBottomTabNavigator()

export default HomeTab = ()=>{
    return(
        <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;
  
            if (route.name === 'Home') {
              iconName = focused
                ? 'home'
                : 'home-outline';
            } else if (route.name === 'Profile') {
              iconName = focused 
                ? 'journal-sharp' 
                : 'journal-outline';
            }
  
            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        
        tabBarOptions={{
          activeTintColor: '#3DDC84',
          inactiveTintColor: 'gray',
        }}>
          
          <Tab.Screen name='Home' component={Home}/>
          <Tab.Screen name="Profile" component={ProfileDrawer}/>


        </Tab.Navigator>
    )
}