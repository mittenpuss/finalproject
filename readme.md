Aplikasi untuk Melihat Foto - Foto Kucing (iya anda tidak salah baca)

Dibuat menggunakan React Native dan menggunakan Redux untuk management state nya

Menggunakan API: https://thecatapi.com/

Link Gitlab: https://gitlab.com/mittenpuss/finalproject

Video Demo Aplikasi dan APK dapat dilihat pada: https://drive.google.com/drive/folders/1yYal-QDOAB-BdsEcHEi1Ecfj7lAivcCU?usp=sharing

Mockup Figma dapat dilihat pada: https://www.figma.com/file/mnT9ST6x6NSXRWjUbp8BXb/Final-Project?node-id=0%3A1

